muleDeploy {
    // version of the tool
    version '1.0'

    apiSpecification {
        name 'web-order-batch-papi'
    }

    
    cloudHubApplication {
        environment params.env
        // the Maven plugin will automatically set `params.appArtifact` to the path of the target JAR if it is used
         workerSpecs {
            muleVersion '4.3.0'
            usePersistentQueues true
            workerType WorkerTypes().micro
            workerCount 1
            awsRegion AwsRegions().uswest1
        }
  
        file params.appArtifact
        cryptoKey "none"
        autoDiscovery {
            clientId "none"
            clientSecret "none"
        }
        cloudHubAppPrefix 'AVIO'
    }
}